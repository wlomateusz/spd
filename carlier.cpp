#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;
bool ifReturn = false;
vector<int> test;
int global=0;       // zm globalna do carliera

void read (vector<vector<int>> &N)
{
    int n=0;
    fstream myfile;
    myfile.open("SCHRAGE5.txt", ios_base::in);


    myfile>>n;
    vector<int> zadanie[n];

    int a,b,c;
    a=b=c=0;
    for (int i=0; i<n; ++i)
    {
        myfile>>a>>b>>c;
        zadanie[i].push_back(a);
        zadanie[i].push_back(b);
        zadanie[i].push_back(c);
        N.push_back(zadanie[i]);
    }
}

void display(vector<vector<int>> &N)
{
    int n=N.size(); // ilosc zadan
    int m=N.at(0).size(); // R P Q
    cout<<"wyswietlam!: "<<n<<" "<<m<<endl;

    //cout<<N[0][0]<<N[0][1]<<N[0][2]<<endl;

    for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
        {
            cout<<N[i][j]<<" ";
        }
        cout<<endl;
    }
}

int lowest_index(const vector<vector<int>> &N, int n) // zwraca indeks najmniejszgo R P lub Q
{
    vector<int> R;

    for(int i=0;i<N.size();++i)
    {
        R.push_back(N[i][n]); // pierwsze to numer zadanie drugie to R P lub Q
        //cout<<R[i]<<" "<<endl; // wyswietla po kolei
    }
    return distance(begin(R),(min_element(begin(R),end(R))));

}

int lowest_value(const vector<vector<int>> &N, int n)
{
    return N[lowest_index(N,n)][n];
}

int highest_index(const vector<vector<int>> &N, int n) // zwraca indeks najmniejszgo R P lub Q
{
    vector<int> R;

    for(int i=0;i<N.size();++i)
    {
        R.push_back(N[i][n]); // pierwsze to numer zadanie drugie to R P lub Q
        //cout<<R[i]<<" "<<endl; // wyswietla po kolei
    }
    return distance(R.begin(), max_element(R.begin(), R.end()));

}

int highest_value(const vector<vector<int>> &N, int n)
{
    return N[highest_index(N,n)][n];
}

int Schrage(vector<vector<int>>N) // Alg Schrage, zwraca Cmax
{
    vector<vector<int>> G;
    vector<int> zadanie;
    vector<vector<int>>pi;
    int t=0,k=0,Cmax=0,e=0;
    while (!G.empty() || !N.empty())
    {
        while(!N.empty() && lowest_value(N,0)<=t)
        {
            e=lowest_index(N,0);
            G.push_back(N[e]);
            N.erase(N.begin()+e);
        }

        if(G.empty())
        {
            t=lowest_value(N,0);
        }
        else
        {
               e=highest_index(G,2);
        zadanie=G[e];

        G.erase(G.begin()+e);

        ++k;t=t+zadanie[1];Cmax=max(Cmax,t+zadanie[2]);
        }



    }
    pi.push_back(zadanie);
    return Cmax;
}
int Schrage_pmtn(vector<vector<int>>N) // Alg preSchrage, zwraca Cmax
{
    vector<vector<int> > G;
    vector<int> zadanie;
    vector<int> akt_zad;


    int t=0;
    int Cmax=0;
    int e=0;
    int q0=INT_MAX;

    akt_zad.push_back(0);akt_zad.push_back(0);akt_zad.push_back(q0);


    while (!G.empty() || !N.empty())
{
        while(!N.empty() && lowest_value(N,0)<=t)
        {
            e=lowest_index(N,0);
            zadanie=N[e];
            G.push_back(N[e]);
            N.erase(N.begin()+e);

            if(zadanie[2]>akt_zad[2])
            {
                akt_zad[1]=t-zadanie[0];
                t=zadanie[0];
            if(akt_zad[1]>0)
                G.push_back(akt_zad);
            }
        }

        if(G.empty())
        {
            t=lowest_value(N,0);
        }
        else
        {
            zadanie=G[highest_index(G,2)];
            G.erase(G.begin()+highest_index(G,2));
            akt_zad=zadanie;t=t+zadanie[1];Cmax=max(Cmax,t+zadanie[2]);
        }
//cout<<"Cmax: "<<Cmax<<endl;
        //cout<<"zadanie "<<k<<": "<<akt_zad[0]<<" "<<akt_zad[1]<<" "<<akt_zad[2]<<endl;

}
    return Cmax;
}
vector<vector<int>> Schrage_pi(vector<vector<int>>N) // Zwraca permutacje z alg Schrage
{
    vector<vector<int>>pi;
    vector<vector<int>> G;
    vector<int> zadanie;
    int t=0,k=0,Cmax=0,e=0;
    while (!G.empty() || !N.empty())
    {
        while(!N.empty() && lowest_value(N,0)<=t)
        {
            e=lowest_index(N,0);
            G.push_back(N[e]);
            N.erase(N.begin()+e);
        }

        if(G.empty())
        {
            t=lowest_value(N,0);
            continue;
        }

        e=highest_index(G,2);
        zadanie=G[e];

        G.erase(G.begin()+e);

        ++k;t=t+zadanie[1];Cmax=max(Cmax,t+zadanie[2]);
        pi.push_back(zadanie);
    }
    return pi;
}

int Cmax_pi(vector<vector<int>>pi) // Funkcja zwraca wartosc Cmax dla danej permutacji
{
    vector<int> zadanie;
    int t=pi[0][0];
    int Cmax=0;
    for(int i=0;i<pi.size();i++)
    {
        zadanie=pi[i];
        t=max(t,zadanie[0]);
        t+=zadanie[1];
        Cmax=max(Cmax,t+zadanie[2]);
    }
    return Cmax;
}


int Carlier(vector<vector<int>>N, int UB)
{
    ++global;
    //cout<<"global: " <<global<<endl;

    vector<vector<int>> opt_pi,pi;
    vector<int> zadanie;

    pi=Schrage_pi(N);

    // pkt 2
    int U=Schrage(N);
    if (U<UB)
    {
        UB=U;
        opt_pi=pi;
    }

    // PKT 3
    int a=INT_MAX,b=-1,c=-1;
    int t=pi[0][0];

    // Wyznaczenie b
    for(int i=0;i<pi.size();i++)
    {
        //int suma=0;
        int Cmax_PI=Cmax_pi(pi); // przypisanie Cmax dla danego pi
        zadanie=pi[i];
        t=max(t,zadanie[0]);
        t+=zadanie[1];
        if(t+zadanie[2]==Cmax_PI)
        {
            if(i>b)     // wybieram tylko najwieksze b, pierwszy zawsze spelniony bo wyzej b=-1
                b=i;
        }
        // wyznaczenie sumy do a
    }

        for(int i=0;i<pi.size();i++)
    {
        int suma=0;
        int Cmax_PI=Cmax_pi(pi); // przypisanie Cmax dla danego pi
        zadanie=pi[i];
        t=max(t,zadanie[0]);
        t+=zadanie[1];
        for(int s=i;s<=b;s++)
        {
            suma+=pi[s][1];
            //suma+=pi[b][2];
        }
        suma+=pi[b][2];
        //cout<<"to suma "<<suma<<endl;
        // wyznaczenie a
        //cout<<"r: "<<zadanie[0]+suma<<" cmax: "<<Cmax_PI<<endl;
        if((zadanie[0]+suma)==Cmax_PI)
        {
            if(i<a)
                a=i;  // wybieram tylko najmniejsze a, pierwszy zawsze spelniony, bo wyzej a=INT_MAX (i rosnie, wiec moze if niepotrzebny??)
        }
    }

    // wyznaczenie c
    for(int i=a;i<=b;i++)
    {
        if(pi[i][2]<pi[b][2])
        {
            if(i>c)
                c=i;
        }
    }

    if(c==-1)
    {
        //return UB;
        ifReturn = true;
    }
    //<<" a b c: "<<a<<b<<c<<endl;
    // PKT 4
    if(ifReturn)
    {
        test.push_back(UB);
        //cout<<"lol"<<endl;
        return UB;
    }



    // PKT 5

    int rp=INT_MAX,qp=INT_MAX,pp=0;
    for(int i=c+1;i<=b;i++)
    {
        if(pi[i][0]<rp)
            rp=pi[i][0];

        if(pi[i][2]<qp)
            qp=pi[i][2];

        pp+=pi[i][1];
    }

    // PKT 6-10
    int r_temp=pi[c][0];
    pi[c][0]=max(pi[c][0],rp+pp);
    int LB=Schrage_pmtn(pi);
    if(LB<UB)
        Carlier(pi,UB);
    pi[c][0]=r_temp;

    // PKT 11-15
    int q_temp=pi[c][2];
    pi[c][2]=max(pi[c][2],qp+pp);
    LB=Schrage_pmtn(pi);
    if(LB<UB)
        Carlier(pi,UB);
    pi[c][2]=q_temp;
    return UB;


}

int main()
{
    vector<vector<int>>N;
    read(N);
    Carlier(N,INT_MAX);
    cout<<"Cmax to: "<<test.at(0)<<endl;
    return 0;
}
