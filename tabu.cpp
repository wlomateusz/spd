#include <iostream>
#include <stdlib.h>
#include <vector>
#include <list>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <string>


using namespace std;

struct Process
{
    int maszyny[100];
    int time;
};


int IleProcesow(vector<Process>& proc)
{
    fstream plik;
    plik.open("neh2.txt");
    int maszyn {};
    int procesy {};
    plik>> maszyn;
    plik>> procesy;
    return procesy;
}
int IleMaszyn(vector<Process>& proc)
{
    fstream plik;
    plik.open("neh2.txt");
    int maszyn {};
    plik>> maszyn;
    return maszyn;
}


void read(vector<Process>& proc)
{
    fstream plik;
    plik.open("neh2.txt");
    if(!plik.good()) cout << "Nie dziala" << endl;

    int procesy {};
    int maszyny {};

    plik>> procesy;
    plik>> maszyny;

    int l=0;
    int time=0;
    while(!plik.eof())
    {
        proc[l].time=l;
        for(int i=0; i<IleMaszyn(proc); ++i)
        {
            plik >> proc[l].maszyny[i];
            time+=proc[l].maszyny[i];
        }
        proc[l].time=time;
        time=0;
        l++;
    }
}

bool operator == (const vector<Process> &a, const vector<Process> &b)
{
    bool val=true;
    for(int i=0; i<a.size(); ++i)
    {
        if(a[i].time!=b[i].time)
            val=false;
    }
    return val;
}



int licz_cmax(vector<Process> &proc,int masz,int zad)
{

    if(masz==0 && zad==0) return proc[zad].maszyny[masz];
    else if(masz==0) return licz_cmax(proc,masz,zad-1)+proc[zad].maszyny[masz];
    else if(zad==0) return licz_cmax(proc,masz-1,zad)+proc[zad].maszyny[masz];
    else return max(licz_cmax(proc,masz,zad-1),licz_cmax(proc,masz-1,zad))+proc[zad].maszyny[masz];
}

int cmax_all(vector<Process> &proc,int masz, int zad)
{
    return licz_cmax(proc,masz-1,zad-1);
}


int TS(vector<Process>proc)
{
    read(proc);
    int maxTabuSize=100;

    vector<Process> sBest =proc; //1-4
    vector<Process> bestCandidate = proc;
    list<vector<Process> > tabuList;
    vector<vector<Process> > neighborList;
    tabuList.push_back(proc);



    while(1)
    {
        for(int i=0; i<proc.size()-1; ++i) //6 sasiedztwo
        {
            for(int j=i+1; j<proc.size(); ++j) {
                swap(bestCandidate[i],bestCandidate[j]);
                neighborList.push_back(bestCandidate);
            }
        }
        bestCandidate=neighborList[0];
        for(int i=0; i<neighborList.size(); ++i) //8
        {
            if(((find(tabuList.begin(),tabuList.end(),neighborList[i])==tabuList.end())) &&
                    (cmax_all(neighborList[i],IleMaszyn(proc),IleProcesow(proc))<cmax_all(bestCandidate,IleMaszyn(proc),IleProcesow(proc)))) //9 tabuList i ifitness
            {
                bestCandidate=neighborList[i]; //10 nowy best na liscieN
            }
        }
        if(cmax_all(bestCandidate,IleMaszyn(proc),IleProcesow(proc)) < cmax_all(sBest,IleMaszyn(proc),IleProcesow(proc)))
            sBest = bestCandidate; //14 czy rozw
        else break;
        tabuList.push_back(bestCandidate); //16-18
        if(tabuList.size()>maxTabuSize)
            tabuList.pop_front();
    }
    return cmax_all(sBest,IleMaszyn(proc),IleProcesow(proc));
}

int main()
{
    vector<Process> proc(IleProcesow(proc));
    cout << TS(proc) << endl;
    return 0;
}
