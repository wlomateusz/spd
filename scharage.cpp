/*
Algorytm Schrage podobnie jak sortR, bierze pod uwage czas przygotowania w wykonywaniu zadan, jednak uzwglednia rowniez czas wykonania.
Zadanie polega na podzieleniu zadań. Zaletą algorytmu jest, ze szybko wykonuja sie zadania, które potrezbuja mniej czasu na przygotowanie.
Sprawdzenie działania poprzez wyświetlenie kolejności wykonywania.
*/


#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

void read (vector<vector<int> > &N)
{
    int n=0;
    fstream myfile;
    myfile.open("SCHRAGE8.txt", ios_base::in);


    myfile>>n;
    vector<int> zadanie[n];

    int a,b,c;
    a=b=c=0;
    for (int i=0; i<n; ++i)
    {

        myfile>>a>>b>>c;
        zadanie[i].push_back(a);
        zadanie[i].push_back(b);
        zadanie[i].push_back(c);
        N.push_back(zadanie[i]);
    }


}

void display(vector<vector<int> > &N)
{
    int n=N.size(); // ilosc zadan
    int m=N.at(0).size(); // R P Q
    cout<<"wyswietlam!: "<<n<<" "<<m<<endl;

    //cout<<N[0][0]<<N[0][1]<<N[0][2]<<endl;

    for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
        {
            cout<<N[i][j]<<" ";
        }
        cout<<endl;
    }
}

int lowest_index(const vector<vector<int> > &N, int n) // zwraca indeks najmniejszgo R P lub Q
{
    vector<int> R;

    for(unsigned int i=0;i<N.size();++i)
    {
        R.push_back(N[i][n]); // pierwsze to numer zadanie drugie to R P lub Q
        //cout<<R[i]<<" "<<endl; // wyswietla po kolei
    }
    return distance(begin(R),(min_element(begin(R),end(R))));

}

int lowest_value(const vector<vector<int> > &N, int n)
{
    return N[lowest_index(N,n)][n];
}

int highest_index(const vector<vector<int> > &N, int n) // zwraca indeks najmniejszgo R P lub Q
{
    vector<int> R;

    for(unsigned int i=0;i<N.size();++i)
    {
        R.push_back(N[i][n]); // pierwsze to numer zadanie drugie to R P lub Q
        //cout<<R[i]<<" "<<endl; // wyswietla po kolei
    }
    return distance(begin(R), max_element(begin(R), end(R)));

}

int highest_value(const vector<vector<int> > &N, int n)
{
    return N[highest_index(N,n)][n];
}

int main()
{

    vector<vector<int> > N;
    vector<vector<int> > G;
    vector<int> zadanie;
    vector<int> akt_zad;

    read(N);
    //display(N);

    int t=0;
    int Cmax=0;
    int e=0;
    int k=0;
    int j =1;
    int q0=INT_MAX;


    akt_zad.push_back(0);akt_zad.push_back(0);akt_zad.push_back(q0);


while (!G.empty() || !N.empty())
{
        while(!N.empty() && lowest_value(N,0)<=t)
        {
            e=lowest_index(N,0);
            G.push_back(N[e]);
            N.erase(N.begin()+e);
        }

        if(G.empty())
        {
            t=lowest_value(N,0);
        }
        else
        {
        e=highest_index(G,2);
        zadanie=G[e];

        G.erase(G.begin()+e);
        akt_zad = zadanie;
        ++k;t=t+zadanie[1];Cmax=max(Cmax,t+zadanie[2]);
        }
        if(j>1)
        cout<<"zadanie :"<<akt_zad[0]<<" "<<akt_zad[1]<<" "<<akt_zad[2]<<endl; //sprawdzenie wykonywalnosci
        j++;



}
        cout<<"Cmax: "<<Cmax<<endl;

    return 0;
}
