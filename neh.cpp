#include <iostream>
#include <fstream>
#include <queue>
#include <algorithm>
#include <iterator>

using namespace std;

struct Process
{
    vector<int> time;
    int sum;
};

void read( vector<Process>& proc)
{
    fstream file;

    file.open("NEH6.txt");

    int procesy {};
    int maszyny {};
// wczytanie m, n:
    file>> procesy;
    file>> maszyny;

//odczyt
    for(int j=0; j<procesy; j++)
    {
        Process infor;
        vector<int> buffor;
        int ja;

        for(int i =0; i<maszyny; i++)
        {
            file>>ja;
            buffor.push_back(ja);
        }
        int i, sum = 0;
        for (i=0; i<buffor.size(); i++)
        {
            sum += buffor[i];
        }
        infor.sum=sum;
        infor.time=buffor;
        proc.push_back(infor);
    }
}

void show(std::vector<Process>& proc)
{
    cout<<"Licza proces�w: "<<proc.size()<<endl;
    for(int i=0; i<proc.size(); i++)
    {
        cout<<"suma zadania numer: "<<i+1;
        if (i<9) cout<<" ";
        cout<<" to: "<<proc[i].sum<<" zadania: ";
        for(int j=0; j<proc[i].time.size(); j++)
        {
            cout<<proc[i].time[j]<<" ";
        }
        cout<<endl;
    }
}

bool porownanie (Process i,Process j)
{
    return (i.sum>j.sum);    // porowanie do sortowania
}



int* licz_cmax(vector<int> zad1, vector<int> zad2, int* w1 )
{
    // cout<<w1[0]<<" "<<w1[1]<<" "<<w1[2]<<" "<<w1[3]<<endl;
    int* w2;
    w2=new int[zad1.size()];
    for(int i=1; i<zad1.size(); i++)
    {
        w2[i]=0;
    }
    int cmax=0;

    w2[0]=w1[0]+zad2[0];
    for(int i=1; i<zad1.size(); i++)
    {
        w2[i]=max(w2[i-1],w1[i])+zad2[i];
    }
    cmax=w2[zad1.size()-1];
    //cout<<"oto cmax: "<<cmax<<endl;
    return w2;

}

int cmax_all(vector<Process> proc)
{
    int* j1;
    j1=new int[proc.at(0).time.size()];
    int* j3;

    for (int i=0; i<proc.size()-1; i++)
    {
        if(i==0)
        {
            j1[0]=proc[0].time.at(0);
            for(int j=1; j<proc.at(0).time.size(); j++)
            {
                j1[j]=proc[0].time.at(j)+j1[j-1];
            }
            j3=licz_cmax(proc[0].time,proc[1].time,j1);
        }
        else
        {
            j3=licz_cmax(proc[i].time,proc[i+1].time,j3);
        }
    }
    delete[] j1;
    return j3[proc[0].time.size()-1];
}

void reswap2(std::vector<Process>& proc, int ilosc)
{
    for(int i=0; i<ilosc; i++)
    {
        iter_swap(proc.begin()+i  , proc.begin()+i+1 );
    }

}

int NEH2(std::vector<Process>& proc)
{
    int cmax;
    int* cmaxy;
    int pos=0;
    vector<Process> temp;
    read(proc);
    cout<<"przed sortowaniem:"<<endl;//wczytany z pliku
    show(proc);
    sort (proc.begin(), proc.end(), porownanie);
    cout<<"po sortowaniu nierosnacym"<<endl;
    show(proc);

    temp.push_back(proc[0]); // wrzucam pierwszy element

    for(int i=1; i< proc.size(); i++)
    {
        cmaxy= new int[temp.size()+1];

        temp.push_back(proc[i]);


        cmaxy[0]=cmax_all(temp);

        for(int j=1; j<temp.size(); j++)
        {
            iter_swap(temp.end() -j-1 , temp.end() -j); //
            cmaxy[j]=cmax_all(temp);
        }

        pos= distance(cmaxy, min_element(cmaxy, cmaxy + temp.size()));
        reswap2(temp,temp.size()-1-pos);

    }
    cmax=cmax_all(temp);
    return cmax;
}

int main ()
{
    vector<Process> proc;
    cout<<NEH2(proc)<<endl;
    return 0;
}
